<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Checkout</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        a {
            color:#000;
        }

        a:hover {
            color: #000;
        }
    </style>
</head>
<body>
<div>
@guest
    @yield('content')
@else
<div class="d-flex justify-content-between">
    <nav class="sidebar">
        <header>
            <div class="image">
                <img src="http://profilepicturesdp.com/wp-content/uploads/2018/06/default-profile-picture-gmail-2.png" class="profile-image">
            </div>
            <h4 class="mt-3 font-weight-bold">{{ Auth::user()->name }}</h4>
        </header>
        <ul class="font-weight-bold">
            <a href="/home"><li><i class="fas fa-shopping-cart"></i> Checkout</li></a>
            <li><i class="fas fa-shopping-cart"></i> Register new user</li>
        </ul>
        <div class="text-center bottom">
            <a  class="logout font-weight-bold" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </nav>
    <div id="app" class="main-page">
      <reg-page/>
    </div>
</div>
@endguest
</div>
</body>
</html>
