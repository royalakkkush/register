import axios from 'axios';

export const placeOrder = async(payload) => {
    const response = await axios.post('order', payload).catch(error => { throw Error(error)});
    if(response.status !== 200) return [];
    return response.data;
}

export const fetchOrders = async() => {
    const response = await axios.get('order').catch(error => { throw Error(error)});
    if(response.status !== 200) return [];
    return response.data;
}

export const fetchCancelOrders = async() => {
    const response = await axios.get('cancel-orders').catch(error => { throw Error(error) });
    if(response.status !== 200) return [];
    return response.data;
}

export const cancelOrder = async(params) => {
    const response = await axios.delete('cancel-order', {params}).catch(error => { throw Error(error) });
    if(response.status !== 200) return [];
    return response.data;
}

export const fetchOrderHistory = async() => {
    const response = await axios.get('order-history').catch(error => { throw Error(error) });
    if(response.status !== 200) return [];
    return response.data;
}