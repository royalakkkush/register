import axios from 'axios';

export const registerUser = async(payload) => {
    const response = await axios.post('register', payload).catch(error => { throw Error(error)});
    return response.data;
}