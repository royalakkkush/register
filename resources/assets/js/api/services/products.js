import axios from 'axios';

export const saveProduct = async(form) => {
    const response = await axios.post('product', form).catch(error => { throw Error(error)});
    if(response.status !== 200) return [];
    return response.data;
}

export const fetchProducts = async() => {
    const response = await axios.get('product').catch(error => { throw Error(error)});
    if(response.status !== 200) return [];
    return response.data;
}

export const updateProduct = async(params) => {
    const response = await axios.put('product', params).catch(error => { throw Error(error)});
    if(response.status !== 200) return [];
    return response.data;
}