<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'Auth\RegisterController@index');

Route::post('/product', 'ProductController@save');
Route::get('/product', 'ProductController@getProduct');
Route::put('/product', 'ProductController@update');
Route::post('order', 'OrderController@save');
Route::get('order', 'OrderController@index');
Route::get('cancel-orders', 'OrderController@cancelOrders');
Route::get('order-history', 'OrderController@history');
Route::delete('cancel-order', 'OrderController@cancelOrder');