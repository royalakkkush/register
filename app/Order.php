<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const ATTR_MEMBER_ID = 'member_id';

    protected $table = 'orders';
    protected $fillable = [
        self::ATTR_MEMBER_ID
    ];

    public function setMemberId($id){
        $this->{ self::ATTR_MEMBER_ID } = $id;
    }    
}
