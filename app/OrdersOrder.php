<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersOrder extends Model
{
    use SoftDeletes;
    const ATTR_ORDER_ID = 'order_id';
    const ATTR_PRODUCT_ID = 'product_id';
    const ATTR_NAME = 'name';
    const ATTR_PRICE = 'price';
    const ATTR_IS_GRAM = 'is_gram';
    const ATTR_QUANTITY = 'quantity';
    const ATTR_TYPE = 'type';
    const ATTR_IS_PRICE = 'is_price';
    
    protected $table = 'orders_to_order';
    protected $fillable = [
        self::ATTR_ORDER_ID,
        self::ATTR_PRODUCT_ID,
        self::ATTR_NAME,
        self::ATTR_PRICE,
        self::ATTR_IS_GRAM,
        self::ATTR_QUANTITY,
        self::ATTR_TYPE,
        self::ATTR_IS_PRICE
    ];

    public function setOrderId($orderId){
        $this->{self::ATTR_ORDER_ID } = $orderId;
    }

    public function setProductID($id){
        $this->{self::ATTR_PRODUCT_ID } = $id;
    }

    public function setName($name){
        $this->{self::ATTR_NAME } = $name;
    }

    public function setPrice($price){
        $this->{self::ATTR_PRICE } = $price;
    }

    public function setIsGram($gram){
        $this->{self::ATTR_IS_GRAM } = $gram;
    }

    public function setQuantity($quantity){
        $this->{self::ATTR_QUANTITY} = $quantity;
    }

    public function setType($type){
        $this->{self::ATTR_TYPE} = $type;
    }

    public function setIsPrice($price){
        $this->{self::ATTR_IS_PRICE} = $price;
    }

    public function orders(){
        return $this->hasMany('App\Order', 'id', 'order_id');
    }
}
