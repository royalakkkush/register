<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const ATTR_PRODUCT_NAME = 'name';
    const ATTR_PRODUCT_PRICE = 'price';
    const ATTR_PRODUCT_IN_STOCK = 'in_stock';
    const ATTR_PRODUCT_IS_GRAM = 'is_gram';
    const ATTR_TYPE = 'type';
    const ATTR_IS_PRICE = 'is_price';

    protected $table = 'product';

    protected $fillable = [
        self::ATTR_PRODUCT_NAME,
        self::ATTR_PRODUCT_PRICE,
        self::ATTR_PRODUCT_IN_STOCK,
        self::ATTR_PRODUCT_IS_GRAM,
        self::ATTR_TYPE,
        self::ATTR_IS_PRICE
    ];

    public function setProductName($name) {
        $this->{ self::ATTR_PRODUCT_NAME } = $name;
    }

    public function setProductPrice($price){
        $this->{ self::ATTR_PRODUCT_PRICE } = $price;
    }

    public function setProductInStock($inStock){
        $this->{ self::ATTR_PRODUCT_IN_STOCK } = $inStock;
    }

    public function setProductIsGram($isGram){
        $this-> { self::ATTR_PRODUCT_IS_GRAM } = $isGram;
    }

    public function setType($type){
        $this->{ self::ATTR_TYPE } = $type;
    }

    public function setIsPrice($price){
        $this->{self::ATTR_IS_PRICE} = $price;
    }
}
