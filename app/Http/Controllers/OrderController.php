<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrdersOrder;
use Carbon\Carbon;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return OrdersOrder::with('orders')->whereDate('created_at', Carbon::today())->get();
    }

    public function cancelOrders(){
        return OrdersOrder::with('orders')->onlyTrashed()->get();
    }

    public function cancelOrder(Request $request){
        $this->validate($request, [
            'id' => 'required'
        ]);

        $order = OrdersOrder::where('id', '=', $request->input('id'))->delete();
    }

    public function history(){
        return OrdersOrder::with('orders')->withTrashed()->get();
    }

    public function save(Request $request){
        $this->validate($request, [
            'member_id' => 'sometimes',
            'free' => 'sometimes',
            'paidAtDesk' => 'sometimes',
            'products' => 'array',
        ]); 

        $orderPerson = new Order();
        $orderPerson->setMemberId($request->input('member_id'));
        if($orderPerson->save()){
            foreach($request->input('products') as $product){
                    $products = new OrdersOrder();
                    $products->setOrderId($orderPerson->id);
                    $products->setProductID($product['id']);
                    $products->setName($product['name']);
                    $products->setPrice($product['price']);
                    $products->setIsGram($product['is_gram']);
                    $products->setQuantity($product['gram']);
                    $products->setType($product['type']);
                    $products->setIsPrice($product['is_price']);
                    $products->save();
            }
            return response($orderPerson, 200);
        }
    }
}
