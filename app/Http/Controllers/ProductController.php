<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function save(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'in_stock' => 'required',
            'is_gram' => 'required',
            'type' => 'required',
            'is_price' => 'required'
        ]);

       $product = new Product();
       $product->setProductName($request->input('name'));
       $product->setProductPrice($request->input('price'));
       $product->setProductInStock($request->input('in_stock'));
       $product->setProductIsGram($request->input('is_gram'));
       $product->setType($request->input('type'));
       $product->setIsPrice($request->input('is_price'));

       if($product->save()){
           return response($product, 200);
       }else{
           return response('500 server error occur', 500);
       }
    }

    public function getProduct(){
        return Product::all();
    }

    public function update(Request $request){
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'in_stock' => 'required',
            'is_gram' => 'required',
            'type' => 'required'
        ]);

        $product = Product::where('id', '=', $request->input('id'))->first();
        $product->setProductName($request->input('name'));
        $product->setProductPrice($request->input('price'));
        $product->setProductInStock($request->input('in_stock'));
        $product->setProductIsGram($request->input('is_gram'));
        $product->setType($request->input('type'));

        if($product->save()){
            return response($product, 200);
        }else{
            return response('500 server error occur', 500);
        }
    }
}
